<?php

namespace Business\Enums;

class RolesEnum extends BaseEnum {

    const User = 1;
    const Visitor = 2;


    public $Descriptions = [];

    public function __construct() {
        $this->Descriptions = [
            1 => "User",
            2 => "Visitor",
            3 => "Admin",

        ];
    }
}
