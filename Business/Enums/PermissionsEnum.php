<?php


namespace Business\Enums;


class PermissionsEnum extends BaseEnum
{
	//region User permissions
	const Dashboard = 1;
	const ViewAlbums = 2;
	const EditAlbum = 3;

}