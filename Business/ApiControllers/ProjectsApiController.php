<?php
namespace Business\ApiControllers;

use Business\Models\ProjectModel;
use Data\DataManagers\ProjectsDataManager;

class ProjectsApiController {

	public static function InsertProject($model) {
		return ProjectsDataManager::InsertProject($model);
	}

	public static function UpdateProject($model) {
		return ProjectsDataManager::UpdateProject($model);
	}

	public static function DeleteProject($projectId) {
		return ProjectsDataManager::DeleteProject($projectId);
	}

	/**
	 * @return ProjectModel[]
	 */
	public static function GetProjects() {
		return ProjectsDataManager::GetProjects();

	}

	/**
	 * @param $keyword
	 * @param null $order
	 * @param null $filter
	 * @return array|\Business\Models\ProjectModel[]
	 */
	public static function SearchProjects($keyword, $order = null, $filter = null) {
		return ProjectsDataManager::SearchProjects($keyword, $order, $filter);
	}

	/**
	 * @return ProjectModel[]
	 */
	public static function GetPublicProjects() {
		return ProjectsDataManager::GetPublicProjects();
	}

	/**
	 * @param $userId
	 * @return ProjectModel[]
	 */
	public static function GetUserProjects($userId) {
		return ProjectsDataManager::GetUserProjects($userId);
	}

	public static function GetSupportedProjects($userId) {
		return ProjectsDataManager::GetSupportedProjects($userId);
	}

	/**
	 * @param $projectId
	 * @return ProjectModel
	 */
	public static function GetProjectById($projectId) {
		return ProjectsDataManager::GetProjectById($projectId);
	}

	public static function GetNewProjects($offset, $limit){
		return ProjectsDataManager::GetNewProjects($offset, $limit);
	}

}