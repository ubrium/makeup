<?php
namespace Business\ApiControllers;


use Business\Models\UserModel;
use Business\Security\Users;
use Data\DataManagers\ConfirmationLinksDataManager;
use Data\DataManagers\UserRolesDataManager;
use Data\DataManagers\UsersDataManager;
use Data\Repositories\UsersRepository;
use DateTime;


class UsersApiController {

	public static function InsertUser($model) {
		return UsersDataManager::InsertUser($model);
	}

	public static function UpdateUser($model) {
		return UsersDataManager::UpdateUser($model);
	}

	public static function DeleteUser($userId) {
		return UsersDataManager::DeleteUser($userId);
	}

	/**
	 * @return UserModel[]
	 */
	public static function GetUsers() {
		return UsersDataManager::GetUsers();
	}

	/**
	 * @return UserModel[]
	 */
	public static function GetAdmins() {
		return UsersDataManager::GetAdmins();
	}

	/**
	 * @param $userId
	 * @return UserModel
	 */
	public static function GetUserById($userId) {
		return UsersDataManager::GetUserById($userId);
	}

	public static function GetUserByUserName($userName) {
		return UsersDataManager::GetUserByUserName($userName);
	}

	public static function GetUserRole($userId) {
		return UserRolesDataManager::GetUserRole($userId);
	}


	public static function InsertUserRole($model) {
		return UserRolesDataManager::InsertUserRole($model);
	}

	public static function DeleteUserRole($userRoleId) {
		UserRolesDataManager::DeleteUserRole($userRoleId);
	}

	public static function InsertPasswordResetLink($passwordResetLink) {
		return UsersDataManager::InsertPasswordResetLink($passwordResetLink);
	}

	public static function GetPasswordResetLink($token) {
		return UsersDataManager::GetPasswordResetLink($token);
	}

	public static function Login($username = null, $password = null, $token = null, $logInFromAdmin = false) {
		if ($token == null) {
			return Users::Login($username, $password, $logInFromAdmin);
		} else {
			return Users::LoginWithToken($token);
		}
	}

	public static function LoginAdmin($username = null, $password = null, $token = null) {
		if ($token == null) {
			return Users::LoginAdmin($username, $password);
		} else {
			return Users::LoginWithToken($token);
		}
	}

	/**
	 * @param integer $userId
	 * @return UserModel
	 */
	public static function GetUser($userId) {
		return UsersRepository::GetOne([UsersRepository::COLUMN_USER_ID => $userId]);
	}


	public static function Update(UserModel $user) {

		return UsersRepository::Update($user);
	}

	public static function GetActiveToken($userId) {
		return UsersDataManager::GetUserAccessToken($userId);
	}


	public static function Logout($userId) {
		return UsersDataManager::RemoveUserAccessToken($userId);
	}


	public static function GetRolePermissions($roleId) {
		return UsersDataManager::GetRolePermissions($roleId);
	}

	public static function CryptPassword($password) {
		return Crypt::HashPassword($password);
	}

	public static function UpdateUserRole($userRole) {
		return UsersDataManager::UpdateUserRole($userRole);
	}

	//region ConfirmationLinks
	public static function CreateConfirmationLink($model) {
		return ConfirmationLinksDataManager::Insert($model);
	}

	/**
	 * @param $userId
	 * @return \Business\Models\UserRoleModel[]
	 */
	public static function GetUserRoles($userId) {
		return UserRolesDataManager::GetUserRoles($userId);
	}

	//endregion

	public static function GetUserByEmail($email) {
		return UsersDataManager::GetUserByEmail($email);
	}

	public static function UpdatePasswordResetLink($model) {
		UsersDataManager::UpdatePasswordResetLink($model);
	}

	public static function ActivateUser($userId) {
		$user = UsersApiController::GetUserById($userId);
		$user->ConfirmRegistration = 1;
		$user->Active = 1;

		UsersApiController::UpdateUser($user);
	}

	/**
	 * @param $guid
	 * @return \Business\Models\ConfirmationLinkModel
	 */
	public static function GetConfirmationLinkByGuid($guid) {
		return ConfirmationLinksDataManager::GetByGuid($guid);
	}

	/**
	 * @param $guid
	 */
	public static function GetUserResetPasswordLink($guid) {
		return UsersDataManager::GetUserResetPasswordLink($guid);
	}

	public static function GetUserResetPasswordLinkByUserId($userId) {
		return UsersDataManager::GetUserResetPasswordLinkByUserId($userId);
	}

	public static function DeleteUserResetPasswordLink($PasswordResetLinkId) {
		return UsersDataManager::DeleteUserResetPasswordLink($PasswordResetLinkId);
	}


	// endregion

}