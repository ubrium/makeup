<?php


namespace Business\DTO;

class CurrentUserDTO {

	public $UserId;
	public $Picture;
	public $Username;
	public $FirstName;
	public $LastName;
	public $Password;
	public $Email;
	public $RoleId;
	public $RegistrationDate;
	public $ConfirmRegistration;

	public function PictureSource() {
		if (!empty($this->Picture)) {
			return sprintf("%sUsers/%s", CDN_URL, $this->Picture);
		}
		return sprintf("%s/Content/roboto-logo.png", CDN_URL);
	}

}