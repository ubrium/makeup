<?php
namespace Business\Models;

/**
 * Class ProjectModel
 * @package Business\Models
 * @property integer $ProjectId
 * @property integer $UserId
 * @property string $Name
 * @property string $Description
 * @property string $Image

 */
class ProjectModel
{
    public $ProjectId;
    public $UserId;
    public $Name;
    public $Description;
    public $Image;
    public $DateCreated;

    public function PicturePath() {
        if (!empty($this->Image)) {
            return sprintf("%s/Media/projects/%s", CDN_URL, $this->Image);
        }
        return sprintf("%s/DefaultImages/defpro.png", CDN_URL);
    }


}