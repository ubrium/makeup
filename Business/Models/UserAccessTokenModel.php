<?php

namespace Business\Models;


class UserAccessTokenModel {

    public $UserAccessTokenId;
    public $Token;
    public $UserId;
    public $StartDate;
    public $EndDate;

    function __construct($UserAccessTokenId = null, $Token = null, $UserId = null, $StartDate = null, $EndDate = null) {
        $this->UserAccessTokenId = $UserAccessTokenId;
        $this->Token = $Token;
        $this->UserId = $UserId;
        $this->StartDate = $StartDate;
        $this->EndDate = $EndDate;
    }

    public function IsActive() {
        return $this->EndDate === null;
    }


}