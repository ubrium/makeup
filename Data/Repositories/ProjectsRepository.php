<?php


namespace Data\Repositories;

use Business\ApiControllers\ProjectsApiController;
use Business\Enums\ProjectsFilterEnum;
use Business\Enums\ProjectsOrderEnum;
use Business\Models\ProjectModel;
use Data\Database\MysqliDb;
use Data\Database\Protocol\Join;
use Data\Database\Protocol\OrWhereDTO;
use Data\Database\Protocol\WhereDTO;
use Security;

/**
 * Class ProjectsRepository
 * @package Data\Repositories
 * @method static ProjectModel[] Get
 * @method static ProjectModel GetOne
 */
class ProjectsRepository extends BaseRepository
{

	public static function Joins()
	{
		return [new Join(new UsersRepository())];
	}

}