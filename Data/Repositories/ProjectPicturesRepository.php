<?php

namespace Data\Repositories;

use Business\DTO\OrderDTO;
use Data\Database\MysqliDb;

class ProjectPicturesRepository extends BaseRepository{

	/**
	 * @param OrderDTO[] $orders
	 */
	public static function UpdatePictureOrder($orders) {
		$db = MysqliDb::getInstance(static::ConnectionName);

		$query = "UPDATE " . self::GetTableName() . " SET `Order` = CASE ProjectPictureId";

		$ids = "";

		foreach ($orders as $order) {
			$order = (object)$order;

			$query .= " WHEN " . $order->ItemId . " THEN " . $order->Order;
			$ids .= "'" . $order->ItemId . "', ";
		}

		$query .= " ELSE `Order` END WHERE ProjectPictureId IN (" . trim($ids, ", ") . ")";

		return $db->rawQuery($query);
	}

	public static function GetMaxOrder($projectId) {
		$db = MysqliDb::getInstance(static::ConnectionName);

		$query = sprintf("SELECT MAX(`Order`) as MaxOrder FROM %s WHERE ProjectId = '%d'", self::GetTableName(), $projectId);
		$rows = $db->rawQuery($query);
		return (int)$rows[0]['MaxOrder'];
	}
}