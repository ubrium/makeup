<?php

namespace Data\Repositories;

use Data\Database\MysqliDb;

class UserAccessTokensRepository extends BaseRepository {

    const COLUMN_USER_ACCESS_TOKEN_ID = 'UserAccessTokenId';
    const COLUMN_TOKEN = 'Token';
    const COLUMN_USER_ID = 'UserId';
    const COLUMN_START_DATE = 'StartDate';
    const COLUMN_END_DATE = 'EndDate';


    public static function RemoveToken($userId) {
        $db = MysqliDb::getInstance();

        $db->where(self::COLUMN_USER_ID, $userId);
        $db->where(self::COLUMN_END_DATE, null);
        return $db->update('user_access_tokens', [
            self::COLUMN_END_DATE => date("Y-m-d H:i:s")
        ]);
    }
}