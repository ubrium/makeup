<?php
namespace Data\Repositories;

use Data\Database\Protocol\Join;
use Data\Database\MysqliDb;
use Data\Database\Protocol\OrWhereDTO;
use Data\Database\Protocol\WhereDTO;
use ReflectionClass;
use \Exception;

class BaseRepository {

    const Table = null;
    const Model = null;
    const PrimaryKey = null;
    const ConnectionName = 'default';

    /**
     * @param null $model
     * @return bool|integer
     * @throws Exception
     */
    public static function Insert($model = null) {
        $db = MysqliDb::getInstance(static::ConnectionName);

        $pk = static::GetPrimaryKey();

        // Filling data from model
        if (is_array($model)) {
            if (count($model) > 0) {
                $sql = "INSERT INTO " . static::GetTableName() . " VALUES";
                $values = [];
                foreach ($model as $modelData) {
                    $value = "(''";
                    $modelReflection = new ReflectionClass($modelData);
                    foreach ($modelReflection->getProperties() as $property) {
                        if (is_scalar($property->getValue($modelData)) or is_null($property->getValue($modelData))) {
                            if ($property->getName() !== $pk) {
                                $value .= "," . $property->getValue($modelData);
                            }
                        }
                    }
                    $value .= ")";
                    $values[] = $value;
                }
                $sql .= implode(",", $values);

                $res = $db->rawQuery($sql);
                if ($res === false) {
                    throw new Exception($db->getLastError());
                }
                return true;
            }
            return true;
        }
        else {
            $data = [];
            $modelReflection = new ReflectionClass($model);
            foreach ($modelReflection->getProperties() as $property) {
                if (is_scalar($property->getValue($model)) or is_null($property->getValue($model))) {
                    if ($property->getName() !== $pk) {
                        $data[$property->getName()] = $property->getValue($model);
                    }
                }
            }

            $insertId = $db->insert(static::GetTableName(), $data);

            if ($insertId == false) {
                throw new Exception($db->getLastError());
            }
            return $insertId;
        }
    }

    public static function Update($model) {
        $db = MysqliDb::getInstance(static::ConnectionName);

        $pk = static::GetPrimaryKey();

        // Filling data from model
        $data = [];
        $modelReflection = new ReflectionClass($model);
        foreach ($modelReflection->getProperties() as $property) {
            if (is_scalar($property->getValue($model)) or is_null($property->getValue($model))) {
                if ($property->getName() !== $pk) {
                    $data[$property->getName()] = $property->getValue($model);
                }
                else {
                    $db->where($property->getName(), $property->getValue($model));
                }
            }
        }

        $success = $db->update(static::GetTableName(), $data);
        return $success;
    }

    public static function Delete($id) {
        $db = MysqliDb::getInstance(static::ConnectionName);

        $pk = static::GetPrimaryKey();

        $db->where($pk, $id);

        $success = $db->delete(static::GetTableName());
        return $success;
    }

    public static function Get($wheres = [], $orders = [], $numRows = null, $columns = null, $groupBy = null, $packToModel = true) {
        $alias = static::GetBaseName();

        $db = MysqliDb::getInstance(static::ConnectionName);
        if (count($wheres) > 0) {
            foreach ($wheres as $key => $where) {
                if ($where instanceof OrWhereDTO) {
                    $db->orWhere($where->Column, $where->Value, $where->Operator);
                }
                elseif ($where instanceof WhereDTO) {
                    $db->where($where->Column, $where->Value, $where->Operator);
                }
                else {

                    if (strpos($key, ".")) {
                        $orderAlias = $key;
                    }
                    else {
                        $orderAlias = sprintf("%s.%s", $alias, $key);
                    }


                    if (is_null($where)) {
                        $db->where($orderAlias, $where, '<=>');
                    }
                    else {
                        $db->where($orderAlias, $where);
                    }
                }
            }
        }
        if (count($orders) > 0) {
            foreach ($orders as $key => $order) {
                if ($order == 'RAND') {
                    $db->orderBy('RAND ()');
                }
                elseif ($key[0] == "~") {
                    $db->orderBy(str_replace("~", "", $key), $order);
                }
                else {
                    if (strpos($key, ".")) {
                        $orderAlias = $key;
                    }
                    else {
                        $orderAlias = sprintf("`%s`.`%s`", $alias, $key);
                    }
                    $db->orderBy($orderAlias, $order);
                }
            }
        }

        $fields = [];
        // Handling joins
        $joins = static::Joins();
        $joinedFields = [];
        if (count($joins) > 0) {
            foreach ($joins as $join) {
                $referencedField = static::FindReferencedField($join);
                $referencingField = static::FindReferencingField($join);
                $refAlias = empty($join->Alias) ? $join->Repository->GetBaseName() : $join->Alias;
                $db->join(sprintf("`%s` `%s`", $join->Repository->GetTableName(), $refAlias), sprintf("%s.%s = %s.%s", $alias, $referencingField, $refAlias, $referencedField), $join->Type);

                $modelReflection = new ReflectionClass($join->Repository->GetModelName());
                foreach ($modelReflection->getProperties() as $property) {
                    $fields[] = sprintf("`%s`.`%s` as %s", $refAlias, $property->getName(), $refAlias . $property->GetName());
                }

                $joinedFields[] = $referencingField;
            }
        }

        $modelReflection = new ReflectionClass(static::GetModelName());
        foreach ($modelReflection->getProperties() as $property) {
            $fields[] = sprintf("`%s`.`%s` as %s", $alias, $property->getName(), $alias . $property->GetName());
        }

        if (!empty($groupBy)) {
            $db->groupBy($groupBy);
        }

        $resultArray = $db->get(sprintf("`%s` `%s`", static::GetTableName(), $alias), $numRows, $columns === null ? $fields : $columns);

        if ($packToModel === false) {
            return $resultArray;
        }

        // Filling models
        $result = [];
        foreach ($resultArray as $resultData) {
            $model = static::Map($resultData);
            $result[] = $model;
        }
        return $result;
    }

    public static function GetOne($wheres = [], $orders = [], $columns = null, $groupBy = null, $packToModel = true) {
        $alias = static::GetBaseName();

        $db = MysqliDb::getInstance(static::ConnectionName);
        if (count($wheres) > 0) {
            foreach ($wheres as $key => $where) {
                if ($where instanceof OrWhereDTO) {
                    $db->orWhere($where->Column, $where->Value, $where->Operator);
                }
                elseif ($where instanceof WhereDTO) {
                    $db->where($where->Column, $where->Value, $where->Operator);
                }
                else {

                    if (strpos($key, ".")) {
                        $orderAlias = $key;
                    }
                    else {
                        $orderAlias = sprintf("`%s`.`%s`", $alias, $key);
                    }

                    if (is_null($where)) {
                        $db->where($orderAlias, $where, '<=>');
                    }
                    else {
                        $db->where($orderAlias, $where);
                    }
                }
            }
        }
        if (count($orders) > 0) {
            foreach ($orders as $key => $order) {
                if ($order == 'RAND') {
                    $db->orderBy('RAND ()');
                }
                elseif ($key[0] == "~") {
                    $db->orderBy(str_replace("~", "", $key), $order);
                }
                else {
                    if (strpos($key, ".")) {
                        $orderAlias = $key;
                    }
                    else {
                        $orderAlias = sprintf("`%s`.`%s`", $alias, $key);
                    }
                    $db->orderBy($orderAlias, $order);
                }
            }
        }

        $fields = [];
        // Handling joins
        $joins = static::Joins();
        $joinedFields = [];
        if (count($joins) > 0) {
            foreach ($joins as $join) {
                $referencedField = static::FindReferencedField($join);
                $referencingField = static::FindReferencingField($join);
                $refAlias = empty($join->Alias) ? $join->Repository->GetBaseName() : $join->Alias;
                $db->join(sprintf("`%s` `%s`", $join->Repository->GetTableName(), $refAlias), sprintf("%s.%s = %s.%s", $alias, $referencingField, $refAlias, $referencedField), $join->Type);

                $modelReflection = new ReflectionClass($join->Repository->GetModelName());
                foreach ($modelReflection->getProperties() as $property) {
                    $fields[] = sprintf("`%s`.`%s` as `%s`", $refAlias, $property->getName(), $refAlias . $property->GetName());
                }

                $joinedFields[] = $referencingField;
            }
        }

        $modelReflection = new ReflectionClass(static::GetModelName());
        foreach ($modelReflection->getProperties() as $property) {
            $fields[] = sprintf("`%s`.`%s` as `%s`", $alias, $property->getName(), $alias . $property->GetName());
        }

        if (!empty($groupBy)) {
            $db->groupBy($groupBy);
        }

        $resultArray = $db->getOne(sprintf("`%s` `%s`", static::GetTableName(), $alias), $columns === null ? $fields : $columns);

        if (is_null($resultArray)) {
            return null;
        }

        if ($packToModel === false) {
            return $resultArray;
        }

        // Filling model
        $model = static::Map($resultArray);
        return $model;
    }

    protected static function PrepareJoins(MysqliDb &$db) {
        $alias = static::GetBaseName();
        $joins = static::Joins();
        if (count($joins) > 0) {
            foreach ($joins as $join) {
                $referencedField = static::FindReferencedField($join);
                $referencingField = static::FindReferencingField($join);
                $refAlias = empty($join->Alias) ? $join->Repository->GetBaseName() : $join->Alias;
                $db->join(sprintf("%s `%s`", $join->Repository->GetTableName(), $refAlias), sprintf("%s.%s = %s.%s", $alias, $referencingField, $refAlias, $referencedField), $join->Type);
            }
        }
    }

    protected static function GetFieldNames($joins = true) {
        $alias = static::GetBaseName();
        $fields = [];

        if ($joins) {
            // Handling joins
            $joins = static::Joins();
            $joinedFields = [];
            if (count($joins) > 0) {
                foreach ($joins as $join) {
                    $referencingField = static::FindReferencingField($join);
                    $refAlias = empty($join->Alias) ? $join->Repository->GetBaseName() : $join->Alias;

                    $modelReflection = new ReflectionClass($join->Repository->GetModelName());
                    foreach ($modelReflection->getProperties() as $property) {
                        $fields[] = sprintf("%s.%s as %s", $refAlias, $property->getName(), $refAlias . $property->GetName());
                    }

                    $joinedFields[] = $referencingField;
                }
            }
        }

        $modelReflection = new ReflectionClass(static::GetModelName());
        foreach ($modelReflection->getProperties() as $property) {
            $fields[] = sprintf("%s.%s as %s", $alias, $property->getName(), $alias . $property->GetName());
        }

        return $fields;
    }

    protected static function Map($data) {
        $modelReflection = new ReflectionClass(static::GetModelName());
        $model = $modelReflection->newInstanceWithoutConstructor();
        $alias = static::GetBaseName();

        $joins = static::Joins();
        $joinedFields = [];
        if (count($joins) > 0) {
            foreach ($joins as $join) {
                $referencingField = static::FindReferencingField($join);
                $joinedFields[] = $referencingField;
            }
        }

        foreach ($modelReflection->getProperties() as $property) {
            if (array_key_exists($alias . $property->getName(), $data)) {
                if (in_array($property->getName(), $joinedFields)) {
                    foreach ($joins as $join) {
                        $repo = $join->Repository;
                        if ($property->getName() == static::FindReferencingField($join)) {
                            $modelName = $repo->GetModelName();
                            $refModelReflection = new ReflectionClass($modelName);
                            $refModel = $refModelReflection->newInstanceWithoutConstructor();
                            foreach ($refModelReflection->getProperties() as $refProperty) {
                                $refAlias = empty($join->Alias) ? $join->Repository->GetBaseName() : $join->Alias;
                                if (array_key_exists($refAlias . $refProperty->getName(), $data)) {
                                    $refModel->{$refProperty->getName()} = $data[$refAlias . $refProperty->getName()];
                                }
                            }
                            $model->{$property->getName()} = $refModel;
                        }
                    }
                }
                else {
                    $model->{$property->getName()} = $data[$alias . $property->getName()];
                }
            }
        }
        return $model;

    }


    public static function Count($wheres = [], $orders = [], $groupBy = null, $numRows = 1, $alias = null) {
        $columns = "COUNT(*)";
        if (!is_null($alias)) {
            $columns .= " AS " . $alias;
        }
        if ($numRows === 1) {
            return self::GetOne($wheres, $orders, $columns, $groupBy, false)[$alias === null ? $columns : $alias];
        }
        return self::Get($wheres, $orders, $numRows, $columns, $groupBy, false);
    }

    /**
     * Returns full name of used Model
     */
    private static function GetFullName() {
        $repo = new ReflectionClass(get_called_class());
        $repoName = $repo->getName();
        $baseName = preg_replace('/sRepository$/', '', $repoName);

        return $baseName;
    }

    protected static function GetBaseName() {
        $parts = explode("\\", static::GetFullName());
        return end($parts);
    }

    public static function GetModelName() {
        if (!is_null(static::Model)) {
            return static::Model;
        }

        return 'Business\\Models\\' . static::GetBaseName() . "Model";
    }

    public static function GetTableName() {
        if (!is_null(static::Table)) {
            return static::Table;
        }

        return static::CamelCaseToUnderscoreLowercase(static::GetBaseName()) . 's';
    }

    private static function CamelCaseToUnderscoreLowercase($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    public static function GetPrimaryKey() {
        if (!is_null(static::PrimaryKey)) {
            return static::PrimaryKey;
        }

        return static::GetBaseName() . 'Id';
    }

    /**
     * @param Join $join
     * @return string
     */
    private static function FindReferencedField($join) {
        if ($join->ReferencedColumn !== null) {
            return $join->ReferencedColumn;
        }
        else {
            $reflection = new ReflectionClass($join->Repository);
            $pkMethod = $reflection->getMethod('GetPrimaryKey');
            $pkMethod->setAccessible(true);
            $pk = $pkMethod->invoke(null);

            return $pk;
        }
    }

    /**
     * @param Join $join
     * @return string
     */
    private static function FindReferencingField($join) {
        if ($join->Column !== null) {
            return $join->Column;
        }
        else {
            // Column name is the same
            return static::FindReferencedField($join);
        }
    }

    /**
     * @return Join[]
     */
    public static function Joins() {
        return [];
    }


}