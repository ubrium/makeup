<?php


namespace Data\DataManagers;


use Data\Repositories\RolesRepository;
use Data\Repositories\UserRolesRepository;

class UserRolesDataManager {

    public static function GetUserRole($userId,$roleId = null) {
        if($roleId === null){
            return UserRolesRepository::GetOne(["UserId" =>$userId]);
        }
        return UserRolesRepository::GetOne(["UserId" =>$userId,"RoleId"=>$roleId]);
    }
    public static function GetUserRoles($userId){

        return UserRolesRepository::Get(["UserId" =>$userId]);
    }

    public static function UpdateUserRole($model){


        return UserRolesRepository::Update($model);
    }

    public static function InsertUserRole($model){


        return UserRolesRepository::Insert($model);
    }

    public static function DeleteUserRole($userRoleId){


        return UserRolesRepository::Delete($userRoleId);
    }

}