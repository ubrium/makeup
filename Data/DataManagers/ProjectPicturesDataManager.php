<?php

namespace Data\DataManagers;

use Data\Repositories\ProjectPicturesRepository;

class ProjectPicturesDataManager {

	public static function GetProjectPictures($projectId) {
		return ProjectPicturesRepository::Get(["ProjectId" => $projectId], ["Order"=>"ASC"]);
	}

	public static function GetOneProjectPictures($projectPictureId) {
		return ProjectPicturesRepository::GetOne($projectPictureId);
	}

	public static function InsertProjectPicture($projectPicture) {
		return ProjectPicturesRepository::Insert($projectPicture);
	}

	public static function UpdateProjectPicture($projectPicture) {
		return ProjectPicturesRepository::Update($projectPicture);
	}

	public static function DeleteProjectPicture($projectPictureId) {
		return ProjectPicturesRepository::Delete($projectPictureId);
	}

	public static function UpdatePictureOrder($orders) {
		return ProjectPicturesRepository::UpdatePictureOrder($orders);
	}

}