<?php

namespace Data\DataManagers;
use Data\Repositories\ProjectsRepository;
use Security;

class ProjectsDataManager
{

	public static function GetProjects()
	{
		return ProjectsRepository::Get([], ["DateCreated" => "DESC"]);
	}

	public static function GetPublicProjects()
	{
		return ProjectsRepository::Get(["Public" => 1]);
	}

	public static function GetUserProjects($userId)
	{
		return ProjectsRepository::Get(["UserId" => $userId]);
	}

	public static function GetSupportedProjects($userId)
	{
		return ProjectsRepository::Get(["UserId" => $userId]);
	}

	public static function GetProjectById($projectId)
	{
		return ProjectsRepository::GetOne(["ProjectId" => $projectId]);
	}

	public static function InsertProject($model)
	{
		return ProjectsRepository::Insert($model);
	}

	public static function UpdateProject($model)
	{
		return ProjectsRepository::Update($model);
	}

	public static function DeleteProject($projectId)
	{
		return ProjectsRepository::Delete($projectId);
	}

	public static function GetNewProjects($offset, $limit)
	{
		return ProjectsRepository::Get([], ["DateCreated" => "DESC"], [$offset, $limit]);
	}

	public static function CountProjects()
	{
		return ProjectsRepository::Count();
	}
}