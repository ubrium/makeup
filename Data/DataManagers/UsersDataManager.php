<?php

namespace Data\DataManagers;

use Business\ApiControllers\SchoolsApiController;
use Business\Enums\SchoolCourseUsersFilterEnum;
use Business\Models\UserAccessTokenModel;
use Business\Security\Tokens;
use Data\Database\Protocol\WhereDTO;
use Data\Repositories\CourseUsersRepository;
use Data\Repositories\CourseUserSubscriptionsRepository;
use Data\Repositories\PasswordResetLinksRepository;
use Data\Repositories\RolePermissionsRepository;
use Data\Repositories\SchoolUsersRepository;
use Data\Repositories\SchoolUserSubscriptionsRepository;
use Data\Repositories\UserAccessTokensRepository;
use Data\Repositories\UserRolesRepository;
use Data\Repositories\UsersRepository;
use Data\Repositories\UserDetailsRepository;
use Data\Repositories\UserStatusesRepository;
use Data\Repositories\VCountUserPaymentsRepository;

class UsersDataManager
{

	public static function GetUsers()
	{
		return UsersRepository::Get();
	}

	public static function GetAdmins() {
		return UsersRepository::GetAdmins();
	}

	public static function GetUserById($userId)
	{
		return UsersRepository::GetOne(["UserId" => $userId]);
	}

	public static function GetUserByUserName($userName)
	{
		return UsersRepository::GetOne(["Username" => $userName]);
	}

	public static function InsertUser($model)
	{
		return UsersRepository::Insert($model);
	}

	public static function UpdateUser($model)
	{
		return UsersRepository::Update($model);
	}

	public static function DeleteUser($userId)
	{
		return UsersRepository::Delete($userId);
	}

	public static function GetRolesPermissions()
	{
		return UsersRepository::Get();
	}

	public static function InsertRolesPermission($model)
	{
		return UsersRepository::Insert($model);
	}

	public static function UpdateRolesPermission($model)
	{
		return UsersRepository::Update($model);
	}

	public static function DeleteRolesPermission($id)
	{
		return UsersRepository::Delete($id);
	}



	// Tokens

	/**
	 * Returns access token for user. Active token is returned if $endDate is omitted. Returns null if no token is found.
	 *
	 * @param int $userId
	 * @param string|null $endDate
	 *
	 * @return \Business\Models\UserAccessTokenModel|null
	 */
	public static function GetUserAccessToken($userId, $endDate = null)
	{
		return UserAccessTokensRepository::GetOne([
			UserAccessTokensRepository::COLUMN_USER_ID => $userId,
			UserAccessTokensRepository::COLUMN_END_DATE => $endDate
		]);
	}

	/**
	 * Generates and saves new Access Token for User. Returns false if failed, or new Token Id if successful.
	 *
	 * @param int $userId
	 * @return int|bool
	 */
	public static function CreateUserAccessToken($userId)
	{

		$token = new UserAccessTokenModel();
		$token->Token = Tokens::CreateToken();
		$token->UserId = $userId;
		$token->StartDate = date("Y-m-d H:i:s");
		$token->EndDate = null;

		return UserAccessTokensRepository::Insert($token);
	}

	/**
	 * Sets EndDate for User Access token which is currently active.
	 *
	 * @param int $userId
	 * @return bool
	 */
	public static function RemoveUserAccessToken($userId)
	{
		return UserAccessTokensRepository::RemoveToken($userId);
	}
	// Users

	/**
	 * @param $userId
	 * @return \Business\Models\UserRoleModel[]
	 */
	public static function GetRoles($userId)
	{
		return UserRolesRepository::Get(["UserId" => $userId]);
	}

	public static function GetRolePermissions($roleId)
	{
		return RolePermissionsRepository::GetPermissions($roleId);
	}

	public static function InsertUserRole($userRole)
	{
		return UserRolesRepository::Insert($userRole);
	}

	public static function UpdateUserRole($userRole)
	{
		return UserRolesRepository::Update($userRole);
	}

	public static function GetUserByEmail($email)
	{
		return UsersRepository::GetOne(["Email" => $email]);
	}

	public static function InsertPasswordResetLink($passwordResetLink)
	{
		return PasswordResetLinksRepository::Insert($passwordResetLink);
	}

	public static function GetPasswordResetLink($token)
	{
		return PasswordResetLinksRepository::GetOne(["Token" => $token]);
	}

	public static function UpdatePasswordResetLink($model)
	{

		return PasswordResetLinksRepository::Update($model);
	}

	public static function GetUserResetPasswordLink($guid)
	{
		return PasswordResetLinksRepository::GetOne(["ResetLink" => $guid]);
	}

	public static function GetUserResetPasswordLinkByUserId($userId)
	{
		return PasswordResetLinksRepository::GetOne(["UserId" => $userId]);
	}

	public static function DeleteUserResetPasswordLink($PasswordResetLinkId)
	{
		return PasswordResetLinksRepository::Delete($PasswordResetLinkId);
	}

	public static function CountUsers(){
		return UsersRepository::Count();
	}
}