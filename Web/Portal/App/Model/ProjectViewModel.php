<?php
use Business\Models\ProjectModel;
use Business\Models\ProjectPictureModel;
use Business\Models\UserModel;
use Business\Models\VActivityModel;

/**
 * Class ProjectsViewModel
 * @property ProjectModel $Project
 * @property ProjectPictureModel[] $ProjectPictures
 */
class ProjectViewModel {
	public $Project;
	public $ProjectPictures;
}