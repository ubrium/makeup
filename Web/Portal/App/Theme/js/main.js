/**
 * Created by mihaj on 10/22/2016.
 */
$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();

    if (scroll > 99) {
        $('.page-header').addClass('scrolled');
        $('.page-section').addClass('scrolled');
    } else {
        $('.page-header').removeClass('scrolled');
        $('.page-section').removeClass('scrolled');
    }
});



$(document).ready(function () {

    if (window.location.hash) {
        var page = window.location.hash;

        $('.iniAnim2b').removeClass('loadAnim2');

        $('.page-section').addClass('closed');
        $(page).removeClass('closed');
        $(page + ' ' + '.box.iniAnim2b').addClass('loadAnim2');
    } else {
    }

    /* waypoints */

    $('.cover-logo')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.iniBoxAnim1')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.iniBoxAnim2')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.iniBoxAnim3')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.iniBoxAnim4')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.iniBoxAnim5')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.iniBoxAnim6')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.borderBottomRight')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.borderRightBottom')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.iniAnim1')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: '90%'
        });

    $('.iniAnim2')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: "90%"
        });

    $('.iniAnim2b')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: "80%"
        });

    $('.iniAnim3')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: 'bottom-in-view'
        });

    $('.iniAnim4')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: 'bottom-in-view'
        });

    $('.iniAnim5')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('animate')
            }
        }, {
            offset: 'bottom-in-view'
        });

    /* tooltip */

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    /*counter*/

    $('.count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

});

$(document).on("click", ".search-open", function (e) {

    e.preventDefault();

    $('.main-nav').toggleClass('hidden');
    $('.search-container').toggleClass('hidden');

});

$(document).on("click", ".page-open", function (e) {

    var page = $(this).data('open');

    $('.iniAnim2b').removeClass('loadAnim2');

    $('.page-section').addClass('closed');
    $(page).removeClass('closed');
    $(page + ' ' + '.box.iniAnim2b').addClass('loadAnim2');

});