<?php

abstract class MVCController {

	/* Properties */

	/* Constructor */
	public function __construct($callFunc = true) {
		if ($callFunc) {
			global $router;
			if (!$router->Action) {
				$router->Action = "Index";
			}

			$functionParameters = array();
			if ($_SERVER['REQUEST_METHOD'] === 'POST') {
				$functionPrefix = "Post";
				$parameterCollection = array_merge($_POST, $_FILES, $router->Parameters);
			}
			else {
				$functionPrefix = "Get";
				$parameterCollection = array_merge($_GET, $_FILES, $router->Parameters);
			}

			$reflector = new ReflectionClass($router->Controller . "Controller");
			$reflectionMethod = $reflector->getMethod($functionPrefix . $router->Action);
			$parameters = $reflectionMethod->getParameters();

			foreach ($parameters as $parameter) {
				$paramName = $parameter->name;
				if ($paramName === "permissions") {
					$permissions = $parameter->getDefaultValue();
					continue;
				}

				if (!isset($parameterCollection[$paramName])) {
					if ($parameter->isDefaultValueAvailable()) {
						$functionParameters[$paramName] = $parameter->getDefaultValue();
					}
					else {
						throw new Exception("Missing required parameter: " . $paramName);
					}
				}
				else {
					if ($paramClass = $parameter->getClass()) {
						$param = new $paramClass->name();
						if (is_array($parameterCollection[$paramName])) {
							foreach ($parameterCollection[$paramName] as $paramPropertyName => $paramPropertyValue) {
								$param->$paramPropertyName = $paramPropertyValue;
							}
						}
						$functionParameters[$paramName] = $param;
					}
					else {
						$functionParameters[$paramName] = $parameterCollection[$paramName];
					}
				}
			}

			if (isset($permissions)) {
				if (!Security::CheckPermissions($permissions)) {
					throw new AccessDeniedException();
				}
			}
			$reflectionMethod->invokeArgs($this, $functionParameters);
		}
	}

	public function RenderView($template = null, $parameters = []) {
		require_once 'Lib/Twig/Autoloader.php';
		Twig_Autoloader::register();

		$loader = new Twig_Loader_Filesystem('View');
		$twig = new Twig_Environment($loader, array(
			'cache' => null
		));

		global $router;
		$twig->addGlobal('Router', $router);
		$twig->addGlobal('Config', new Config());
		$twig->addGlobal('Helper', new MainHelper());

		if (is_null($template)) {
			$template = $this->_getCaller();
		}

		$template = $twig->loadTemplate($template . '.twig');

		echo $template->render($parameters);


	}

	private function _getCaller() {
		$trace = debug_backtrace();
		$controller = preg_replace('/Controller$/', '', $trace[2]['class']);
		$method = preg_replace('/^Get|^Post/', '', $trace[2]['function']);
		return ($controller . "/" . $method);
	}

}