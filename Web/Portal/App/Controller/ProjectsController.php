<?php
use Business\ApiControllers\ProjectPicturesApiController;
use Business\ApiControllers\ProjectsApiController;


class ProjectsController extends MVCController{

	public function GetProject($projectId){

		$model = new ProjectViewModel();

		$model->Project = ProjectsApiController::GetProjectById($projectId);
		$model->ProjectPictures = ProjectPicturesApiController::GetProjectPictures($projectId);

		$this->RenderView("Projects/Project", ["model" => $model]);
	}


}