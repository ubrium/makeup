<?php


use Business\ApiControllers\ProjectsApiController;

class HomeController extends MVCController {

	public function GetIndex() {

		$model = new LandingViewModel();

		$model->Projects = ProjectsApiController::GetProjects();

		$this->RenderView("Landing/Landing", ["model" => $model]);

	}

}