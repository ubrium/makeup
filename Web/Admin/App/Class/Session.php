<?php

abstract class Session
{

    private static $Started = false;

    public static function Start()
    {
        if (empty($_SESSION) && !static::$Started) {
            session_start();
            static::$Started = true;
        }
    }

    public static function Set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function Get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return false;
        }
    }

    public static function Remove($key) {
        unset($_SESSION[$key]);
    }

    public static function Destroy() {
        session_destroy();
        static::$Started = false;
    }

}