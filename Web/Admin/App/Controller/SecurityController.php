<?php
use Business\ApiControllers\UsersApiController;
use Business\DTO\CurrentUserDTO;
use Business\Enums\PermissionsEnum;
use Business\Enums\RolesEnum;
use Business\Models\ConfirmationLinkModel;
use Business\Models\UserModel;
use Data\DataManagers\UsersDataManager;

class SecurityController extends MVCController
{

	public function GetLogin()
	{
		if (Cookie::Exists("UserAccessToken")) {
			$user = UsersApiController::Login(null, null, Cookie::Get("UserAccessToken"));
			$this->LoginUser($user, Cookie::Get("UserAccessToken"));
			return;
		}
		$this->_renderLoginPage();
	}


	public function PostLogin($username, $password) {
		$user = UsersApiController::Login($username, $password);
		$this->LoginUser($user);
		Router::Redirect("dashboard");
	}

	public function GetLogout()
	{
		/** @var UserModel $user */
		$user = Security::GetCurrentUser();

		if (UsersApiController::Logout($user->UserId)) {

			Session::Remove("CurrentUser");
			Cookie::Delete("UserAccessToken");

			Router::Redirect("login");
		} else {

			Router::Redirect("dashboard");
		}
	}

	private function _renderLoginPage()
	{
		$this->RenderView("Home/Login");
	}

	public function PostValidateUsername($username)
	{
		$user = UsersApiController::GetUserByUserName($username);

		if ($user === null) {
			$userExists = false;
		} else {
			$userExists = true;
		}

		echo json_encode(
			(object)[
				"Success" => $userExists
			]
		);
	}

	public function PostValidateEmail($email)
	{
		$user = UsersApiController::GetUserByEmail($email);

		if ($user === null) {
			$userExists = false;
		} else {
			$userExists = true;
		}

		echo json_encode(
			(object)[
				"Success" => $userExists
			]
		);
	}


	/**
	 * @param UserModel|null $user
	 * @param string|null $activeToken
	 * @param null|integer $timeout
	 */
	private function LoginUser($user, $activeToken = null, $timeout = null)
	{
		$admin = false;

		if (!is_null($user)) {

			$userRoles = UsersDataManager::GetRoles($user->UserId);

			$rolePermissions = [];

			foreach ($userRoles as $userRole) {
				$oneRolePermissions = UsersDataManager::GetRolePermissions($userRole->RoleId);
				$rolePermissions = array_merge($rolePermissions, $oneRolePermissions);

				if ($userRole->RoleId == RolesEnum::User) {
					$admin = true;
				}
			}

			if ($admin == true) {

				if (is_null($activeToken)) {
					$activeToken = UsersApiController::GetActiveToken($user->UserId)->Token;
				}
				if (!is_null($activeToken)) {
					Cookie::Set("UserAccessAdminToken", $activeToken, Cookie::SevenDays);
				}

				// Packing CurrentUserDTO from user
				$userDto = new CurrentUserDTO();

				$userDto->Username = $user->Username;
				$userDto->Email = $user->Email;
				$userDto->UserId = $user->UserId;

				$userDetails = UsersApiController::GetUserById($user->UserId);

				$userDto->FirstName = $userDetails->FirstName;
				$userDto->LastName = $userDetails->LastName;

				$userDto->Permissions = array_unique($rolePermissions);

				Security::SetCurrentUser($userDto);

				if (!is_null($timeout)) {
					$route = sprintf("refresh:5;url=%s", Router::Create("dashboard", [], false));
					header($route);
				} else {
					Router::Redirect('dashboard');
				}
			} else {
				$this->_renderLoginPage();
			}
		} else {
			$this->_renderLoginPage();
		}
	}



} 