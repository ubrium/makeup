<?php

use Business\ApiControllers\UsersApiController;
use Business\DTO\RecipientDTO;
use Business\DTO\SenderDTO;
use Business\Enums\RolesEnum;
use Business\Helper\NotificationHelper;
use Business\Models\UserModel;
use Business\Models\UserRoleModel;
use Business\Security\Crypt;

class UserController extends MVCController {


	public function GetDashboard() {
		$this->RenderView("User/Dashboard");
	}

	public function GetRegister() {
		$this->RenderView("Register/Register");
	}

	public function PostRegister($firstName, $email, $password) {

		$userModel = new UserModel;

		$userModel->FirstName = $firstName;
		$userModel->Email = $email;
		$userModel->Password = Crypt::HashPassword($password);
		$userModel->RegistrationDate = date("Y-m-d H:i:s");


		$userId = UsersApiController::InsertUser($userModel);

		$userRoleModel = new UserRoleModel();

		$userRoleModel->UserId = $userId;
		$userRoleModel->RoleId = RolesEnum::User;

		UsersApiController::InsertUserRole($userRoleModel);

		Router::Redirect("dashboard");

	}




}