<?php
use Business\ApiControllers\ProjectPicturesApiController;
use Business\ApiControllers\ProjectsApiController;
use Business\Enums\PermissionsEnum;
use Business\Models\ProjectModel;
use Business\Models\ProjectPictureModel;
use Data\Repositories\ProjectPicturesRepository;

class ProjectsController extends MVCController {

	public function GetProjects($permissions = [PermissionsEnum::ViewAlbums]) {

		$model = new ProjectsViewModel();

		$model->Projects = ProjectsApiController::GetProjects();

		$this->RenderView("Projects/Projects", ["model" => $model]);
	}

	public function GetEditProject($projectId, $permissions = [PermissionsEnum::EditAlbum]) {
		$model =  new ProjectViewModel();

		$model->Project =ProjectsApiController::GetProjectById($projectId);
		$model->ProjectPictures =ProjectPicturesApiController::GetProjectPictures($projectId);

		$this->RenderView("Projects/EditProject", ["model" => $model]);
	}

	public function GetDeleteProject($projectId) {

		ProjectsApiController::DeleteProject($projectId);

		Router::Redirect("projects");
	}

	public function PostEditProject($title, $description, $picture = null, $projectId) {

		$project = ProjectsApiController::GetProjectById($projectId);

		$project->Name = $title;
		$project->Description = $description;

		if ($picture['name'] !== "") {
			$newName = self::_generatePictureName($picture['name'], $title);
			move_uploaded_file($picture['tmp_name'], self::_generatePictureFullPath($newName));
			$project->Image = $newName;
		}

		ProjectsApiController::UpdateProject($project);

		Router::Redirect("edit-project", ["projectId" => $projectId]);
	}

	public static function PostUploadProjectPicture($pictures, $projectId)
	{

		$response = [];

		$count = count($pictures['name']);

		$maxOrder = ProjectPicturesRepository::GetMaxOrder($projectId);

		for ($i = 0; $i < $count; $i++) {
			$projectPicture = new ProjectPictureModel();

			if ($pictures['name'][$i] !== "") {
				$newName = self::_generateProjectPictureName($pictures['name'][$i]);
				move_uploaded_file($pictures['tmp_name'][$i], self::_generateProjectPictureFullPath($newName));
				$projectPicture->Picture = $newName;
			}
			$projectPicture->ProjectId = $projectId;
			$projectPicture->UploadDate = date("Y-m-d H:i:s");
			$projectPicture->Order = ++$maxOrder;

			$pictureId = ProjectPicturesApiController::InsertProjectPicture($projectPicture);

			$dto = new PictureUrlsDTO();
			$dto->UrlFull = $projectPicture->PictureUrl();
			$dto->UrlThumb = $projectPicture->PictureUrl();            //use for smaller (thumb) images
			$dto->PictureId = $pictureId;
			$response[] = $dto;
		}

		echo json_encode($response);
	}

	public static function GetDeleteProjectPicture($projectPictureId)
	{
		$projectPicture = ProjectPicturesApiController::GetOneProjectPictures($projectPictureId);
		ProjectPicturesApiController::DeleteProjectPicture($projectPictureId);
		unlink(sprintf("%s/Media/projects/ProjectPictures/%s", CDN_PATH, $projectPicture->Picture));
	}

	public static function PostOrderProjectPictures($orders)
	{
		ProjectPicturesApiController::UpdatePicturesOrder($orders);
	}

	public function PostCreateProject($title, $description, $picture = null) {
		$currentUser = Security::GetCurrentUser();
		$model = new ProjectModel();
		$model->Name = $title;
		$model->Description = $description;
		$model->DateCreated = date("Y-m-d H:i:s");
		$model->UserId = $currentUser->UserId;
		if ($picture['name'] !== "") {
			$newName = self::_generatePictureName($picture['name'], $title);
			move_uploaded_file($picture['tmp_name'], self::_generatePictureFullPath($newName));
			$model->Image = $newName;
		}
		ProjectsApiController::InsertProject($model);
		Router::Redirect("dashboard");
	}

	private static function _generatePictureFullPath($pictureName) {
		return sprintf("%sMedia/projects/%s", CDN_PATH, $pictureName);
	}

	private static function _generatePictureName($logo, $name) {
		return CommonHelper::StringToFilename(sprintf("project-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($logo)));
	}

	private static function _generateProjectPictureName($picture)
	{
		return CommonHelper::StringToFilename(sprintf("project-picture-%s.%s", CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($picture)));
	}


	private static function _generateProjectPictureFullPath($pictureName)
	{
		return sprintf("%s/Media/projects/ProjectPictures/%s", CDN_PATH, $pictureName);
	}
}