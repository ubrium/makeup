<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 04-Sep-15
 * Time: 13:30
 */


class PictureUrlsDTO {

	public $UrlThumb;
	public $UrlFull;
	public $PictureId;

}