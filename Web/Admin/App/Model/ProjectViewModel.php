<?php
use Business\Models\ProjectModel;
use Business\Models\ProjectPictureModel;

/**
 * Class ProjectsViewModel
 * @property ProjectModel $Project
 * @property ProjectPictureModel[] $ProjectPictures

 */
class ProjectViewModel {

	public $Project;
	public $ProjectPictures = [];

}