<?php

abstract class MVCModel {

    public $State;
    public $Errors = array();

    public function ValidateInt($x) {
        return is_int($x);
    }

    public function ValidateNumber($x) {
        return is_numeric($x);
    }

    public function ValidateLowerThan($x, $limit) {
        return $x < $limit;
    }

    public function ValidateGreaterThan($x, $limit) {
        return $x > $limit;
    }

    public function ValidateEqualTo($x, $limit) {
        return $x === $limit;
    }

    public function GetErrorMessage($key) {
        if(array_key_exists($key, $this->Errors)){
            return $this->Errors[$key];
        } else{
            return false;
        }
    }

}


