<?php


class CommonHelper {


    public static function GetExtension($fileName) {
        return pathinfo($fileName, PATHINFO_EXTENSION);

    }

    public static function GenerateRandomString($length = 10) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    public static function StringToFilename($str) {
        $str = str_replace(["ž", "Ž", "č", "Č", "ć", "Ć", "đ", "Đ", "š", "Š"], ["z", "Z", "c", "C", "c", "C", "dj", "Dj", "s", "S"], $str);
        $str = strip_tags($str);
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode($str, ENT_QUOTES, "utf-8");
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
        $str = str_replace(' ', '-', $str);
        $str = rawurlencode($str);
        $str = str_replace('%', '-', $str);
        return $str;
    }

    public static function SortShopsByName($a, $b){
        return strcmp($a->Name, $b->Name);
    }
}