<?php
define('STOCKROOM', 1);
define("CDN_PATH", dirname(__FILE__) . "/Web/CDN/");
define("CDN_URL", "http://cdn.makeup.dev/");
define("ADMIN_URL", "http://admin.makeup.dev/");
define("PORTAL_URL", "http://makeup.dev/");
// Languages and gettext
define('PROJECT_DIR', realpath('./'));
define('LOCALE_DIR', PROJECT_DIR . '/Locale');
define('DEFAULT_LOCALE', 'de_DE');
define('DEFAULT_LANGUAGE', 'de');