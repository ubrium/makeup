/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : makeup

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-12-20 13:25:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `PermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `Caption` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`PermissionId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'Dashboard', null);
INSERT INTO `permissions` VALUES ('2', 'ViewAlbums', null);
INSERT INTO `permissions` VALUES ('3', 'EditAlbum', null);

-- ----------------------------
-- Table structure for `projects`
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `ProjectId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `Name` varchar(250) COLLATE utf8_bin NOT NULL,
  `Image` varchar(250) COLLATE utf8_bin NOT NULL,
  `Description` text COLLATE utf8_bin NOT NULL,
  `DateCreated` datetime NOT NULL,
  PRIMARY KEY (`ProjectId`),
  KEY `UserId` (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES ('5', '7', 'title', 'project-title-abdo3xt8f2.jpg', 0x090909090909090909, '2016-12-02 16:11:00');
INSERT INTO `projects` VALUES ('6', '7', 'mala krsta', 'project-da-li-menjas-naslov12-aju53wrhjb.jpg', 0x090909090909090909096D6E20626E6D623839380909090909090D0A0909090909090D0A0909090909090909090D0A0909090909090909090D0A0909090909090909090D0A0909090909090909090D0A0909090909090909090D0A090909090909090909, '2016-12-13 14:39:55');
INSERT INTO `projects` VALUES ('7', '7', 'Treci test', 'project-treci-test-s3uyyzrrnx.jpg', 0x090909090909090909096F766F206A6520747265636920746573740909090909090909090D0A090909090909090909, '2016-12-16 13:58:37');
INSERT INTO `projects` VALUES ('8', '7', 'j', 'project-j-pdzjvg5usb.jpg', 0x09096A09090909090909, '2016-12-20 12:21:07');
INSERT INTO `projects` VALUES ('9', '7', 'rth', 'project-rth-35bt0xpkpy.jpg', 0x0909090909676866676809090909, '2016-12-20 12:21:45');

-- ----------------------------
-- Table structure for `project_pictures`
-- ----------------------------
DROP TABLE IF EXISTS `project_pictures`;
CREATE TABLE `project_pictures` (
  `ProjectPictureId` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectId` int(11) DEFAULT NULL,
  `Picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UploadDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ProjectPictureId`),
  KEY `project_pictures_ibfk_1` (`ProjectId`) USING BTREE,
  CONSTRAINT `project_pictures_ibfk_1` FOREIGN KEY (`ProjectId`) REFERENCES `projects` (`ProjectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of project_pictures
-- ----------------------------

-- ----------------------------
-- Table structure for `referrals`
-- ----------------------------
DROP TABLE IF EXISTS `referrals`;
CREATE TABLE `referrals` (
  `ReferralsId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `Name` varchar(250) COLLATE utf8_bin NOT NULL,
  `Position` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ReferralsId`),
  KEY `fk_referrals_users` (`UserId`),
  CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of referrals
-- ----------------------------

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `Protected` tinyint(4) NOT NULL,
  `Active` tinyint(4) NOT NULL,
  `Description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '1', '1', 'User');
INSERT INTO `roles` VALUES ('2', '1', '1', 'Visitor');
INSERT INTO `roles` VALUES ('3', '1', '1', 'Admin');

-- ----------------------------
-- Table structure for `role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions` (
  `RolePermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) NOT NULL,
  `PermissionId` int(11) NOT NULL,
  `Protected` smallint(6) NOT NULL,
  PRIMARY KEY (`RolePermissionId`),
  KEY `FK_RolePermissions_Permissions` (`PermissionId`) USING BTREE,
  KEY `FK_RolePermissions_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `role_permissions_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permissions_ibfk_2` FOREIGN KEY (`PermissionId`) REFERENCES `permissions` (`PermissionId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES ('1', '1', '1', '1');
INSERT INTO `role_permissions` VALUES ('2', '1', '2', '1');
INSERT INTO `role_permissions` VALUES ('9', '2', '1', '1');
INSERT INTO `role_permissions` VALUES ('10', '3', '1', '1');
INSERT INTO `role_permissions` VALUES ('11', '3', '2', '1');
INSERT INTO `role_permissions` VALUES ('12', '3', '3', '1');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `UserId` int(10) NOT NULL AUTO_INCREMENT,
  `Email` varchar(250) NOT NULL,
  `Password` varchar(250) NOT NULL,
  `FirstName` varchar(250) NOT NULL,
  `LastName` varchar(250) DEFAULT NULL,
  `Picture` varchar(250) DEFAULT NULL,
  `RegistrationDate` datetime NOT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `ConfirmRegistration` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `UniqueEmail` (`Email`) USING BTREE,
  UNIQUE KEY `UnigueUsername` (`Username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'ivana@ivana.com', '$2a$10$UDTtUB2RjJ6Qs.VVa3Vko.arr9JTYKSRohSI5RCWSLYyc7o0Co/pG', 'ivana', null, null, '2016-11-18 14:03:21', 'ivana', '1');

-- ----------------------------
-- Table structure for `user_access_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `user_access_tokens`;
CREATE TABLE `user_access_tokens` (
  `UserAccessTokenId` int(11) NOT NULL AUTO_INCREMENT,
  `Token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UserId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime DEFAULT NULL,
  PRIMARY KEY (`UserAccessTokenId`),
  UNIQUE KEY `Unique_Token` (`Token`) USING BTREE,
  KEY `FK_UserAccessTokens_Users` (`UserId`) USING BTREE,
  CONSTRAINT `user_access_tokens_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=429 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_access_tokens
-- ----------------------------
INSERT INTO `user_access_tokens` VALUES ('260', '2e0edf27f81949a9', '68', '2016-06-21 18:02:17', null);
INSERT INTO `user_access_tokens` VALUES ('261', 'f813fa81788209b5', '68', '2016-06-23 11:14:14', null);
INSERT INTO `user_access_tokens` VALUES ('262', 'dfeba2901ed85581', '68', '2016-06-24 12:11:26', null);
INSERT INTO `user_access_tokens` VALUES ('263', 'ea66bb455f41406f', '68', '2016-06-29 16:04:58', null);
INSERT INTO `user_access_tokens` VALUES ('264', 'b278800d8d6a5652', '68', '2016-06-30 12:27:06', null);
INSERT INTO `user_access_tokens` VALUES ('265', '790d94c189479da3', '68', '2016-06-30 16:36:22', null);
INSERT INTO `user_access_tokens` VALUES ('266', '3b604039fe276710', '68', '2016-06-30 16:38:33', null);
INSERT INTO `user_access_tokens` VALUES ('267', '97d05dd13538a2c5', '68', '2016-07-01 10:06:54', null);
INSERT INTO `user_access_tokens` VALUES ('268', '41429f86b58ee213', '68', '2016-07-05 15:41:10', null);
INSERT INTO `user_access_tokens` VALUES ('269', 'e0aada0b1646a3e2', '68', '2016-07-06 13:06:50', null);
INSERT INTO `user_access_tokens` VALUES ('270', 'fef45342400fcd20', '68', '2016-07-06 16:02:24', null);
INSERT INTO `user_access_tokens` VALUES ('271', 'a0552d224ee7fc08', '68', '2016-07-06 17:56:57', null);
INSERT INTO `user_access_tokens` VALUES ('272', '1a4109be3e0529e1', '68', '2016-07-07 12:32:15', null);
INSERT INTO `user_access_tokens` VALUES ('273', 'f9c2111795886ce9', '68', '2016-07-07 15:33:01', null);
INSERT INTO `user_access_tokens` VALUES ('274', '60663d10c86886e0', '68', '2016-07-08 12:14:46', null);
INSERT INTO `user_access_tokens` VALUES ('275', '1a7275c5740fed74', '68', '2016-07-12 13:04:56', null);
INSERT INTO `user_access_tokens` VALUES ('276', 'bed0db95c445502f', '68', '2016-07-12 15:18:28', null);
INSERT INTO `user_access_tokens` VALUES ('277', '626f08d4430034ac', '68', '2016-07-12 15:21:53', null);
INSERT INTO `user_access_tokens` VALUES ('278', 'bcb148435cc48860', '68', '2016-07-12 15:54:18', null);
INSERT INTO `user_access_tokens` VALUES ('279', 'ec94f5079f4d81b4', '68', '2016-07-13 12:37:55', null);
INSERT INTO `user_access_tokens` VALUES ('280', '4eadeb120bdbb32c', '68', '2016-07-14 10:00:26', null);
INSERT INTO `user_access_tokens` VALUES ('281', '70c4c473d2f97fbc', '68', '2016-07-14 17:27:32', null);
INSERT INTO `user_access_tokens` VALUES ('282', '3e439ed031e15859', '68', '2016-07-18 14:39:16', null);
INSERT INTO `user_access_tokens` VALUES ('283', '1c26dd1dd93481d0', '68', '2016-07-19 10:39:03', null);
INSERT INTO `user_access_tokens` VALUES ('284', '0fef2650278bb6e2', '68', '2016-07-19 11:29:26', null);
INSERT INTO `user_access_tokens` VALUES ('285', '8d7eb278f55df1bf', '68', '2016-07-19 12:37:28', null);
INSERT INTO `user_access_tokens` VALUES ('286', 'ddd889c37c095724', '68', '2016-07-20 10:32:52', null);
INSERT INTO `user_access_tokens` VALUES ('287', 'b6152bc0321c379c', '68', '2016-07-20 11:46:06', null);
INSERT INTO `user_access_tokens` VALUES ('288', '33ba69b98a8f7d21', '68', '2016-07-20 11:51:05', null);
INSERT INTO `user_access_tokens` VALUES ('289', '9a7d624c944f0129', '68', '2016-07-20 11:53:15', null);
INSERT INTO `user_access_tokens` VALUES ('290', 'c81f319a2c386a33', '68', '2016-07-20 11:54:48', null);
INSERT INTO `user_access_tokens` VALUES ('291', 'df1f9798843235b9', '68', '2016-07-20 11:55:34', null);
INSERT INTO `user_access_tokens` VALUES ('292', '1bcb99740c43a9d6', '68', '2016-07-20 11:55:57', null);
INSERT INTO `user_access_tokens` VALUES ('293', 'c1b0b896cd3677a5', '68', '2016-07-20 11:57:11', null);
INSERT INTO `user_access_tokens` VALUES ('294', 'fbd9f7be993239a0', '68', '2016-07-20 11:57:17', null);
INSERT INTO `user_access_tokens` VALUES ('295', '10c4f32d6e0190e1', '68', '2016-07-20 12:04:29', null);
INSERT INTO `user_access_tokens` VALUES ('296', '431119eda20c83a8', '68', '2016-07-20 19:08:01', null);
INSERT INTO `user_access_tokens` VALUES ('297', '3abda7a42e796d86', '68', '2016-07-20 19:29:17', null);
INSERT INTO `user_access_tokens` VALUES ('298', '79418755dc535da8', '68', '2016-07-22 11:19:27', null);
INSERT INTO `user_access_tokens` VALUES ('299', '4de4c877af3ca329', '68', '2016-07-23 20:22:46', null);
INSERT INTO `user_access_tokens` VALUES ('300', '182c012673b47008', '68', '2016-07-24 14:54:50', null);
INSERT INTO `user_access_tokens` VALUES ('301', '0d8c8569cb670fa2', '68', '2016-07-25 11:50:56', null);
INSERT INTO `user_access_tokens` VALUES ('302', 'd502611475a18f32', '68', '2016-07-25 12:19:52', null);
INSERT INTO `user_access_tokens` VALUES ('303', 'c138574767cfe029', '68', '2016-07-25 13:07:04', null);
INSERT INTO `user_access_tokens` VALUES ('304', '9514a9309396731a', '68', '2016-07-25 15:33:54', null);
INSERT INTO `user_access_tokens` VALUES ('305', 'd987479832d1b72e', '68', '2016-07-25 15:51:55', null);
INSERT INTO `user_access_tokens` VALUES ('306', '93903d80ec0c391b', '68', '2016-07-25 17:21:57', null);
INSERT INTO `user_access_tokens` VALUES ('307', '44f93face25c7e5c', '68', '2016-07-25 17:34:35', null);
INSERT INTO `user_access_tokens` VALUES ('308', '68606b3a449bd6ed', '68', '2016-07-26 10:38:41', null);
INSERT INTO `user_access_tokens` VALUES ('309', '41404cc1296a3bcc', '68', '2016-07-26 11:08:34', null);
INSERT INTO `user_access_tokens` VALUES ('310', '387a4a6a6b77f610', '68', '2016-07-26 11:33:11', null);
INSERT INTO `user_access_tokens` VALUES ('311', '78292082fed3d69e', '68', '2016-07-26 12:31:25', null);
INSERT INTO `user_access_tokens` VALUES ('312', '50b140e8a1001a1b', '68', '2016-07-26 12:42:16', null);
INSERT INTO `user_access_tokens` VALUES ('313', '0a9e78a74e424720', '68', '2016-07-26 14:37:26', null);
INSERT INTO `user_access_tokens` VALUES ('314', 'e3ec4408299aafdd', '68', '2016-07-26 17:13:36', null);
INSERT INTO `user_access_tokens` VALUES ('315', '2b7b389728b55e61', '69', '2016-07-26 17:14:33', null);
INSERT INTO `user_access_tokens` VALUES ('316', '76e83322e15c720d', '68', '2016-07-27 11:29:14', null);
INSERT INTO `user_access_tokens` VALUES ('317', '3a7e049533fed8c3', '68', '2016-07-27 21:18:44', null);
INSERT INTO `user_access_tokens` VALUES ('318', '5f9d413ca6c2c587', '70', '2016-07-28 11:27:44', null);
INSERT INTO `user_access_tokens` VALUES ('319', '8f061a8045d6f632', '71', '2016-07-28 11:48:07', null);
INSERT INTO `user_access_tokens` VALUES ('320', 'e5c7848b64de2f5a', '68', '2016-08-02 12:49:55', null);
INSERT INTO `user_access_tokens` VALUES ('321', '19f56bb0ec9ed420', '68', '2016-08-06 15:12:26', null);
INSERT INTO `user_access_tokens` VALUES ('322', '7d251877af78dc07', '68', '2016-08-07 14:34:31', null);
INSERT INTO `user_access_tokens` VALUES ('323', '383e76d4a5a32c66', '68', '2016-08-08 15:28:14', null);
INSERT INTO `user_access_tokens` VALUES ('324', 'fc0876c254f2dbdb', '68', '2016-08-08 15:29:11', null);
INSERT INTO `user_access_tokens` VALUES ('325', '15a8051b5c4bf15f', '68', '2016-08-08 15:31:08', null);
INSERT INTO `user_access_tokens` VALUES ('326', 'b5bdfd33a629d96c', '68', '2016-08-08 15:33:34', null);
INSERT INTO `user_access_tokens` VALUES ('327', '557c15c89b26689a', '68', '2016-08-08 15:33:41', null);
INSERT INTO `user_access_tokens` VALUES ('328', '8073a4854cdebcd2', '68', '2016-08-08 15:34:25', null);
INSERT INTO `user_access_tokens` VALUES ('329', '8510cc50bda814f1', '68', '2016-08-08 15:34:30', null);
INSERT INTO `user_access_tokens` VALUES ('330', '3d5c40e796418cd2', '68', '2016-08-08 15:34:48', null);
INSERT INTO `user_access_tokens` VALUES ('331', '884d5cb7e541fc63', '68', '2016-08-08 16:02:29', null);
INSERT INTO `user_access_tokens` VALUES ('332', '68089ec16553a630', '68', '2016-08-08 16:05:30', null);
INSERT INTO `user_access_tokens` VALUES ('333', 'fafb2b79d5f498f9', '68', '2016-08-08 16:06:50', null);
INSERT INTO `user_access_tokens` VALUES ('334', '950fc18842683f99', '69', '2016-08-08 16:10:24', null);
INSERT INTO `user_access_tokens` VALUES ('335', 'b0c9d002cd202fc5', '68', '2016-08-08 16:15:24', null);
INSERT INTO `user_access_tokens` VALUES ('336', '55bbbb6317b38407', '68', '2016-08-08 16:19:32', null);
INSERT INTO `user_access_tokens` VALUES ('337', '009e67ddce06f36d', '69', '2016-08-08 16:21:12', null);
INSERT INTO `user_access_tokens` VALUES ('338', '8f5cc278fbb7b8e8', '68', '2016-08-08 16:24:42', null);
INSERT INTO `user_access_tokens` VALUES ('339', '47a394d2bc0e4de8', '68', '2016-08-08 16:28:05', null);
INSERT INTO `user_access_tokens` VALUES ('340', '57d3b8939e7eee93', '68', '2016-08-08 17:15:22', null);
INSERT INTO `user_access_tokens` VALUES ('341', '17607fe923d90aa1', '68', '2016-08-09 11:46:47', null);
INSERT INTO `user_access_tokens` VALUES ('342', '01cf0e86f325d062', '68', '2016-08-09 22:32:07', null);
INSERT INTO `user_access_tokens` VALUES ('343', '69d9a7500c8ba484', '68', '2016-08-11 10:07:20', null);
INSERT INTO `user_access_tokens` VALUES ('344', '3b223c099442b36e', '68', '2016-08-11 13:02:56', null);
INSERT INTO `user_access_tokens` VALUES ('345', '168236de3e492771', '72', '2016-08-11 14:15:19', null);
INSERT INTO `user_access_tokens` VALUES ('346', '2cab03351941b087', '68', '2016-08-11 15:27:21', null);
INSERT INTO `user_access_tokens` VALUES ('347', '0c752837070a0b19', '68', '2016-08-16 14:06:18', null);
INSERT INTO `user_access_tokens` VALUES ('348', '7208cff478bb2cca', '68', '2016-08-18 15:11:04', null);
INSERT INTO `user_access_tokens` VALUES ('349', 'c79ccce6a676d16f', '68', '2016-08-18 15:12:18', null);
INSERT INTO `user_access_tokens` VALUES ('350', '839e9d2b8845eea1', '68', '2016-08-19 12:56:57', null);
INSERT INTO `user_access_tokens` VALUES ('351', '62771b08e01acf87', '68', '2016-08-19 12:58:47', null);
INSERT INTO `user_access_tokens` VALUES ('352', 'fc28233e33711d11', '68', '2016-08-20 17:50:14', null);
INSERT INTO `user_access_tokens` VALUES ('353', '5a70e4811c5f3729', '68', '2016-08-20 18:37:17', null);
INSERT INTO `user_access_tokens` VALUES ('354', '7c424d7a5898d609', '68', '2016-08-22 15:49:15', null);
INSERT INTO `user_access_tokens` VALUES ('355', '72b696b3a7abc52d', '68', '2016-08-22 18:18:02', null);
INSERT INTO `user_access_tokens` VALUES ('356', '8098f35c5e68bfdc', '68', '2016-08-22 23:21:09', null);
INSERT INTO `user_access_tokens` VALUES ('357', '66c971996cc5d7c1', '73', '2016-08-22 23:33:28', null);
INSERT INTO `user_access_tokens` VALUES ('358', 'ad09f5144389571d', '68', '2016-08-23 11:19:27', null);
INSERT INTO `user_access_tokens` VALUES ('359', 'd8ac948d07549dc3', '68', '2016-08-23 11:31:44', null);
INSERT INTO `user_access_tokens` VALUES ('360', '611857b883b522cc', '68', '2016-08-23 18:34:33', null);
INSERT INTO `user_access_tokens` VALUES ('361', '343821da4a0f4a5e', '68', '2016-08-24 14:05:50', null);
INSERT INTO `user_access_tokens` VALUES ('362', '5c66c0983b02391f', '68', '2016-08-24 15:09:13', null);
INSERT INTO `user_access_tokens` VALUES ('363', '3a52f8c1513ae3be', '68', '2016-08-25 11:00:47', null);
INSERT INTO `user_access_tokens` VALUES ('364', '271b7d17b8404fe3', '68', '2016-08-25 14:44:05', null);
INSERT INTO `user_access_tokens` VALUES ('365', '10c4e579311a303c', '68', '2016-08-25 16:19:29', null);
INSERT INTO `user_access_tokens` VALUES ('366', '07b883ef564bae46', '68', '2016-08-25 16:26:07', null);
INSERT INTO `user_access_tokens` VALUES ('367', '20767a924a8c3453', '68', '2016-08-26 11:45:10', null);
INSERT INTO `user_access_tokens` VALUES ('368', '34bdb0c07fc47d0b', '68', '2016-09-07 12:03:57', null);
INSERT INTO `user_access_tokens` VALUES ('369', '9e7592d327429779', '68', '2016-09-07 18:32:24', null);
INSERT INTO `user_access_tokens` VALUES ('370', '9ca02c3f2311259f', '68', '2016-09-08 12:05:26', null);
INSERT INTO `user_access_tokens` VALUES ('371', '1e3dd646e7dc5041', '68', '2016-09-09 13:13:52', null);
INSERT INTO `user_access_tokens` VALUES ('372', '20a6dbc09acb6ed2', '68', '2016-09-09 14:42:18', null);
INSERT INTO `user_access_tokens` VALUES ('373', '69da4a9afc1140f5', '68', '2016-09-09 16:16:21', null);
INSERT INTO `user_access_tokens` VALUES ('374', '9be80a2765d7780f', '68', '2016-09-12 12:03:37', null);
INSERT INTO `user_access_tokens` VALUES ('375', 'a601e8c1ea948324', '68', '2016-09-13 14:01:06', null);
INSERT INTO `user_access_tokens` VALUES ('376', '86f0c5f318dfa773', '68', '2016-09-14 12:51:06', null);
INSERT INTO `user_access_tokens` VALUES ('377', '0138eceed480330e', '68', '2016-09-14 16:13:45', null);
INSERT INTO `user_access_tokens` VALUES ('378', 'c20796a3e76324bf', '68', '2016-09-16 14:09:29', null);
INSERT INTO `user_access_tokens` VALUES ('379', 'b53de0574f47d24c', '68', '2016-09-16 14:35:50', null);
INSERT INTO `user_access_tokens` VALUES ('380', 'ed5f0dce365a92b2', '68', '2016-09-19 11:59:29', null);
INSERT INTO `user_access_tokens` VALUES ('381', '0dda1e69118aa657', '68', '2016-09-20 12:20:20', null);
INSERT INTO `user_access_tokens` VALUES ('382', '4e80683e579bd15b', '68', '2016-09-21 11:07:24', null);
INSERT INTO `user_access_tokens` VALUES ('383', '0dbf1cdee64ebdf4a611bc0b984cea49', '68', '2016-10-11 16:25:46', null);
INSERT INTO `user_access_tokens` VALUES ('384', 'a84007d38f853325b8a7d2b9896e5a69', '68', '2016-10-12 15:02:47', null);
INSERT INTO `user_access_tokens` VALUES ('385', '4a06c8454c8fb6bff570cb867209b63e', '68', '2016-10-18 13:17:12', null);
INSERT INTO `user_access_tokens` VALUES ('386', '668b54f602242932dfecdfbff6e82c78', '68', '2016-10-19 11:53:37', null);
INSERT INTO `user_access_tokens` VALUES ('387', 'b7cb1ec99c1e83dc6e003db4a075891f', '1', '2016-11-18 14:35:51', null);
INSERT INTO `user_access_tokens` VALUES ('388', '5ffd7cd97acc8b6dda21cd36af599e53', '1', '2016-11-18 16:55:43', null);
INSERT INTO `user_access_tokens` VALUES ('389', 'fbefbb219fe6eab50ccb10ebe4eee469', '1', '2016-11-22 13:23:37', null);
INSERT INTO `user_access_tokens` VALUES ('390', '044e1d08b7c493f1ea3fc8ec0141f062', '1', '2016-11-29 13:53:01', null);
INSERT INTO `user_access_tokens` VALUES ('391', '46370f481e5d8b17a2bdc47decb80954', '1', '2016-12-01 15:16:56', null);
INSERT INTO `user_access_tokens` VALUES ('392', 'ec97fd8e6d4b2da3acd5f5f4c8c7ecb8', '1', '2016-12-01 15:51:47', null);
INSERT INTO `user_access_tokens` VALUES ('393', '3c5044bd8fc375325e80507ccc81c6e1', '1', '2016-12-01 15:52:21', null);
INSERT INTO `user_access_tokens` VALUES ('394', '395cac08e7ed2f48e45b527ab2ccdcc3', '1', '2016-12-02 16:10:26', null);
INSERT INTO `user_access_tokens` VALUES ('395', '5b9c80cefcc609fa4e50914b5b15e735', '1', '2016-12-12 12:41:43', null);
INSERT INTO `user_access_tokens` VALUES ('396', '7b3b4e14011f82cd7a9d8fc86748f95d', '1', '2016-12-12 14:36:37', null);
INSERT INTO `user_access_tokens` VALUES ('397', '2623b5fb9808eec8aa8d1c9fd8c72215', '1', '2016-12-12 14:43:03', null);
INSERT INTO `user_access_tokens` VALUES ('398', 'cf00db1bb4ea75da1a93ac7a5ddef41a', '1', '2016-12-12 14:45:54', null);
INSERT INTO `user_access_tokens` VALUES ('399', '0a7766d8ddf32c0c8492fa8c45ef2b09', '1', '2016-12-12 14:49:53', null);
INSERT INTO `user_access_tokens` VALUES ('400', '00a80a2a2079076c331ecb7929fd3bec', '1', '2016-12-12 14:50:42', null);
INSERT INTO `user_access_tokens` VALUES ('401', 'd891037989d874e0ffe338bdb0ef27f2', '1', '2016-12-12 15:00:20', null);
INSERT INTO `user_access_tokens` VALUES ('402', 'e2d4f707317ba500d509bd58b2da069c', '1', '2016-12-12 15:02:48', null);
INSERT INTO `user_access_tokens` VALUES ('403', '67fa5864c5434d76032c1f7bcfb37f86', '1', '2016-12-12 15:05:45', null);
INSERT INTO `user_access_tokens` VALUES ('404', '1176243c75c8c18dcdb40a482ed118a0', '1', '2016-12-13 12:44:43', null);
INSERT INTO `user_access_tokens` VALUES ('405', '1944c61fa093ac6987eef8dae660005f', '1', '2016-12-13 12:57:50', null);
INSERT INTO `user_access_tokens` VALUES ('406', '4c98bee38916a1e85e67bd1db2cb0579', '1', '2016-12-13 13:00:26', null);
INSERT INTO `user_access_tokens` VALUES ('407', '242fdc3b62d5d62884f62d742d3f6d01', '1', '2016-12-13 13:45:05', null);
INSERT INTO `user_access_tokens` VALUES ('408', '945830fd28c32bbcfe4efcd0ee9113ef', '1', '2016-12-13 13:46:08', null);
INSERT INTO `user_access_tokens` VALUES ('409', 'b9b7fe3dabf53d026628ab57c2f39e71', '1', '2016-12-13 14:04:56', null);
INSERT INTO `user_access_tokens` VALUES ('410', 'd76922f9d328f4d5eb9dcb5fc609befc', '1', '2016-12-13 14:05:02', null);
INSERT INTO `user_access_tokens` VALUES ('411', 'b4b3b615bd7f477f6c57f49ef514d76d', '1', '2016-12-13 14:05:06', null);
INSERT INTO `user_access_tokens` VALUES ('412', '47df26da2e4b552ecdef5c47154b32c8', '1', '2016-12-13 14:39:44', null);
INSERT INTO `user_access_tokens` VALUES ('413', '454f4ef1b00a262c0631504d65762c22', '1', '2016-12-14 14:29:23', null);
INSERT INTO `user_access_tokens` VALUES ('414', '455c3e6b5f9f4c08fd4353607cefe443', '1', '2016-12-14 14:31:38', null);
INSERT INTO `user_access_tokens` VALUES ('415', '69efcd3412e203acc931f2328a8c4366', '1', '2016-12-14 14:35:35', null);
INSERT INTO `user_access_tokens` VALUES ('416', '7cd513ecfe05209ec4dcc626d6a5eff4', '1', '2016-12-14 14:37:56', null);
INSERT INTO `user_access_tokens` VALUES ('417', '6d558a6b1ccce911b98bb5ee2053c7d8', '1', '2016-12-14 14:38:23', null);
INSERT INTO `user_access_tokens` VALUES ('418', 'd5b5f151dff832e8634922779489d226', '1', '2016-12-14 14:41:57', null);
INSERT INTO `user_access_tokens` VALUES ('419', '6efd41aac9f9e2a1aa4670a88ada300e', '1', '2016-12-14 14:59:29', null);
INSERT INTO `user_access_tokens` VALUES ('420', 'f3d69ba0c7c40d2db933a6576f2f05e4', '1', '2016-12-15 13:01:12', null);
INSERT INTO `user_access_tokens` VALUES ('421', 'ec0c4431218cb3d0a96d46caae81f637', '1', '2016-12-15 14:25:40', null);
INSERT INTO `user_access_tokens` VALUES ('422', '25c0b2e759c232e2feec10de69b3966e', '1', '2016-12-15 15:27:12', null);
INSERT INTO `user_access_tokens` VALUES ('423', 'e83d20d415f7739ddeea632cb389e17f', '1', '2016-12-16 13:56:08', null);
INSERT INTO `user_access_tokens` VALUES ('424', 'b15b7deab1b8152de4a88a516b4a6cde', '1', '2016-12-16 13:56:36', null);
INSERT INTO `user_access_tokens` VALUES ('425', '90048cf85ce24380e9822ce1962f027f', '1', '2016-12-16 13:57:37', null);
INSERT INTO `user_access_tokens` VALUES ('426', '74166ad3098de135edf7875d0d798d54', '1', '2016-12-19 11:31:45', null);
INSERT INTO `user_access_tokens` VALUES ('427', '8d5469ffdc2f9e3c2d701d804d54e284', '1', '2016-12-20 12:20:32', null);
INSERT INTO `user_access_tokens` VALUES ('428', 'e26adb3a42ae69803848cb1367272120', '1', '2016-12-20 13:19:39', null);

-- ----------------------------
-- Table structure for `user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `UserRoleId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  PRIMARY KEY (`UserRoleId`),
  KEY `FK_UserRoles_Users` (`UserId`) USING BTREE,
  KEY `FK_UserRoles_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('168', '1', '1');
INSERT INTO `user_roles` VALUES ('169', '1', '2');
INSERT INTO `user_roles` VALUES ('171', '1', '3');
